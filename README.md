**El repositorio incluye distintos tipos de análisis y visualización de datos.**

Hola, gracias por revisar el repositorio. El repositorio será parte de varios proyectos de análisis y visualización de datos.

---

## Analisis de Chat de WhatsApp *Quiero ser Telmito*

En este proyecto vamos a analizar el chat de whatsapp que solemos usar todos los días. Aquí vamos a utilizar un grupo de chat que es **"Grupo universitario de diferentes códigos de la UNI"** y en este grupo normalmente intercambiaremos conocimientos que aprendemos, debates, y otros.

1. Análisis de la mayor cantidad de mensajes por usuario.
2. Cantidad, porcentaje de emojis utilizados por usuario.
3. Horas más activa en WhatsApp.
4. Meses de mensajes más altos, y el mes más ocupado.
5. WorldCloud

### Resultados
1. El numéro de mensajes más alto es de 6028, además son pocos los usuarios que interactuan en el grupo de WhatsApp
2. Los Emojis más utilizado son: 🤣, 😂, 🤔, 🥺 y 🤭.
3. Los usuarios del grupo de WhatsApp interactúan más al medio día y a las 8:00 pm.
>> Es el horario de almuerzo y la hora que muchas de las clases en la facultad acaban.
4. Los meses de mayor interacción son: Marzo, Abril y Noviembre.
>> En los meses de marzo y abril es la época de la matrícula y genera mucha interacción entre los chicos y los meses de noviembre las clases acaban y son los meses críticos para muchos porque es la época de los examenes finales y sustiturios. 
5. xd, mano, gente

---

## New project